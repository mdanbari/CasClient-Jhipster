(function() {
    'use strict';

    angular
        .module('jhipsterCasApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
